//secciones del DOM
var home, records, credits, play;

//constante de desplazamiento
const MOVEMENT = 60;
const R = 20;
const Y = -100;


//Inicializacion de las variables de secciones
home = this.document.getElementById('home');
records = this.document.getElementById('record');
credits = this.document.getElementById('credits');
play = this.document.getElementById('play');

//variables de infoHome
var info = document.getElementById('info_home');
var btnCredits = document.getElementById('creditos_home');
var btnRecord = document.getElementById('record_home');
var btnPlay = document.getElementById('play_home');
var popUp = document.getElementById('popUp');

//variables para regresar de los menus
var backFromRecord = document.getElementById('back_from_record');
var backFromCredits = document.getElementById('back_from_credits');

//variables para mover el personaje en el campo de juego
var mvLeft = document.getElementById('mvLeft');
var mvRight = document.getElementById('mvRight');

//valores inciales del campo de juego
var canvas = document.getElementById("play_field");
var pause = document.getElementById('pause');
var pausePopUp = document.getElementById('pausePopUp');
var gameLost = document.getElementById('gameLost');
var btnRecordPause = document.getElementById('record_pause');
var btnRestartGame = document.getElementById('restart_game');
var play_again = document.getElementById('play_again');

var ctx = canvas.getContext('2d');
var pts = document.getElementById('pts');
var puntos = 0;
var cw = canvas.width, ch = canvas.height;
var numMeteors = Math.floor(cw / 55);

//iniciales
var meteors = [];
var imgs = [];
var andy = new andyPlayer();
var imgRock = new Image();
imgRock.src = "./img/roca.png";
imgs.push(imgRock);
var imgMeteor = new Image();
imgMeteor.src = "./img/meteorito.png";
imgs.push(imgMeteor);




//funcion invocada por el botón de creditos
function creditos() {
    home.classList.remove('show');
    home.classList.add('hide');

    credits.classList.remove('hide');
    credits.classList.add('show');
}


//funcion invocada por el botón de record
function goToRecord(section) {
    records.classList.remove('hide');
    records.classList.add('show');
    records.classList.add(section);

    document.getElementById(section).classList.remove('show');
    document.getElementById(section).classList.add('hide');
}

let animateInterval;
//funcion invocada por el botón play
function playGame() {
    home.classList.remove('show');
    home.classList.add('hide');

    play.classList.remove('hide');
    play.classList.add('show');
    initAndy();
    animate();

}
//funcion regresar
function goBack(section) {
    var sectionVariables = document.getElementById(section).classList;
    sectionVariables.forEach((secClass) => {
        if (secClass === "home") {
            document.getElementById("home").classList.remove('hide');
            document.getElementById("home").classList.add('show');
            document.getElementById(section).classList.remove("home");
        } else if (secClass === "play") {
            document.getElementById("play").classList.remove('hide');
            document.getElementById("play").classList.add('show');
            document.getElementById(section).classList.remove("play");
        }
    })
    document.getElementById(section).classList.remove('show');
    document.getElementById(section).classList.add('hide');



}

//funcion info
function infoHome() {
    if (info.style.color === 'rgb(2, 236, 127)') {
        info.style.color = '#2B1842';
        info.src = "img/boton-informacion.png";

        popUp.classList.remove('show');
        popUp.classList.add('hide'); popUp.classList.remove('show');
        popUp.classList.add('hide');

    } else {
        info.style.color = '#02EC7F';
        info.src = "img/boton-informacion-presionado.png";
        popUp.classList.remove('hide');
        popUp.classList.add('show');

        btnCredits.classList.remove('hide');
        btnCredits.classList.add('show');

        btnRecord.classList.remove('hide');
        btnRecord.classList.add('show');
    }

}

//clase jugador
function andyPlayer() {
    this.x = cw / 2;
    this.y = ch / 1.3;
    this.size = 8;

}
//funcion que hace desplzar el personaje el valor de la constante de desplazamiento
function move(valToMove) {
    andy.x += valToMove;
    if (andy.x + 20 > cw) {
        andy.x -= valToMove;
    } else if (andy.x < 0) {
        andy.x -= valToMove;
    }
}
let imgAndy = new Image();
function initAndy() {
    imgAndy.src = "./img/fantasma-partida-normal.png";
    ctx.shadowColor = "rgba(255,255,255,0.5)";
    ctx.shadowBlur = 20;
}




for (var i = 1; i <= numMeteors; i++) {
    var met = new Meteor();
    var img = imgs[(Math.floor((Math.random() * 2) + 1)) - 1];
    met.img = img;
    met.id = i;
    met.x = ((cw / numMeteors) * i) - 30;
    met.y = Math.floor((Math.random() * Y) + -10);
    met.r = R;
    met.k = 0;
    met.type = 0;
    met.lvl = 1;
    meteors.push(met);
}

//Clase meteoro
function Meteor() {
    this.img, this.id, this.x, this.y, this.r, this.lvl, this.type;
    this.render = function (ctx, x, y, R) {
        if (y > ch) {
            this.k = 0;
            this.y = Math.floor((Math.random() * Y) + -10);
            this.img = imgs[(Math.floor((Math.random() * 2) + 1)) - 1];
            newLvl(this);
        }
        ctx.drawImage(this.img, x, y, R, R);
    }
}

//cambia el nivel de los meteoros
function newLvl(meteorToChange) {
    switch (roundPts(puntos)) {
        case 5:
            meteorToChange.lvl = 1.2;
            break;
        case 10:
            meteorToChange.lvl = 1.4;

            break;
        case 20:
            meteorToChange.lvl = 1.6;
            canvas.style.background = 'linear-gradient(#810F28, #9565C1)';
            mvLeft.style.background = 'linear-gradient(#810F28, #9565C1)';
            mvRight.style.background = 'linear-gradient(#810F28, #9565C1)';
            ctx.clearRect(0, 0, cw, ch);
            break;
        case 35:
            meteorToChange.lvl = 2;
            break;
    }
}
//animationFrame 
var animateFrameRequest;
//function que anima lo objetos del juego
function animate() {
    ctx.clearRect(0, 0, cw, ch);
    ctx.drawImage(imgAndy, andy.x, andy.y, 25, 15);

    //meteoros
    meteors.forEach((element) => {
        element.k += 1;
        element.render(ctx, element.x, (element.y + element.k) * element.lvl, R);
    });
    animateFrameRequest = requestAnimationFrame(animate);
    collitions();
    puntos += 1;
    pts.textContent = roundPts(puntos);
}

//redondea el valor de los puntos a segundos
function roundPts() {
    return Math.floor(puntos / 60);
}

//funcion para detectar colisiones
function collitions() {
    meteors.forEach((meteor) => {
        let xDistance = andy.x - meteor.x;
        let yDistance = andy.y - ((meteor.y + meteor.k) * meteor.lvl);

        var hit = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
        var t = (andy.size / 2 + meteor.r / 2);
        if (hit <= t) {
            cancelAnimationFrame(animateFrameRequest);
            gameLost.classList.remove('hide');
            gameLost.classList.add('show');
            document.getElementById('puntaje').textContent = roundPts(puntos) + " Segundos";
        }
    });
}

//pausar el juego
function pauseGame() {
    cancelAnimationFrame(animateFrameRequest);
    mvLeft.style.zIndex = -1;
    mvRight.style.zIndex = -1;
    pausePopUp.classList.remove('hide');
    pausePopUp.classList.add('show');
}

//retoma el juego
function restartGame() {
    mvLeft.style.zIndex = 0;
    mvRight.style.zIndex = 0;
    pausePopUp.classList.remove('show');
    pausePopUp.classList.add('hide');
    setTimeout(() => {
        animateFrameRequest = requestAnimationFrame(animate);
    }, 100);
}

//volver a crear una nueva partida
function goPlayAgain() {
    meteors.forEach((met) => {
        met.y = Math.floor((Math.random() * Y) + -10);
        met.lvl = 1;
        met.k = 0;
        ctx.clearRect(0, 0, cw, ch);
        met.render(ctx, met.x, (met.y + met.k) * met.lvl, R);
    });
    gameLost.classList.remove('show');
    gameLost.classList.add('hide');
    puntos = 0;
    canvas.style.background = 'linear-gradient(#1f5956, #F7D64B)';
    mvLeft.style.background = 'linear-gradient(#1f5956, #F7D64B)';
    mvRight.style.background = 'linear-gradient(#1f5956, #F7D64B)';
    setTimeout(() => {
        animateFrameRequest = requestAnimationFrame(animate);
    }, 500);
}

window.addEventListener('load', function () {
    //botones home 
    info.onclick = infoHome;
    btnCredits.onclick = creditos;
    btnPlay.onclick = playGame;

    //game options
    pause.onclick = pauseGame;
    btnRestartGame.onclick = restartGame;
    play_again.onclick = goPlayAgain;

    //botones movilidad personaje
    mvLeft.addEventListener('click', function () {
        move(-MOVEMENT);
    });

    mvRight.addEventListener('click', function () {
        move(MOVEMENT);
    });

    backFromRecord.addEventListener('click', function () {
        goBack('record');
    });
    backFromCredits.addEventListener('click', function () {
        goBack('creditos');
    });

    btnRecord.addEventListener('click', function () {
        goToRecord('home');
    });
    btnRecordPause.addEventListener('click', function () {
        goToRecord('play');
    });
});
